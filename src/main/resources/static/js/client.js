const client = new StompJs.Client({
    brokerURL: 'ws://' + window.location.host + '/general',
    /*    debug: function (str) {
            console.log(str);
        },*/
    reconnectDelay: 5000,
    heartbeatIncoming: 4000,
    heartbeatOutgoing: 4000,
});

onmessage = function (messageJson) {
    const message = JSON.parse(messageJson.body);
    var messageElement = document.getElementById('main-message');
    messageElement.innerText = message.message;
    messageElement.className = 'main-message ' + message.type + '-message';
};

const subscriptions = [];

client.onConnect = function (frame) {
    const subscription = client.subscribe('/topic/messages', onmessage)
    subscriptions.push(subscription);
    console.log('Subscribed to /topic/messages with #' + subscription.id);
}

function disconnect() {
    subscriptions.forEach(function(subscription) {
        subscription.unsubscribe();
        console.log('Unsubscribed from #' + subscription.id);
    });
    client.deactivate();
}

client.activate();
