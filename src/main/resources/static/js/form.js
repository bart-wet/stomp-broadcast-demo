function sendMessageForm(formId) {
    const formData = {
      message: document.getElementsByName('message')[0].value,
      type: document.getElementsByName('messagetype')[0].value,
    };
    console.log(formData);
    client.publish({ destination: '/topic/general', body: JSON.stringify(formData) });
    console.log('Published message to /topic/general')
    document.getElementById(formId).reset();
}
