package nl.capgemini.wet.stomp.broadcast.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.capgemini.wet.stomp.broadcast.BroadcastService;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@RequiredArgsConstructor
public class BroadcastServiceImpl implements BroadcastService {

    public static final String CLEAN_MESSAGE = "{\"message\":\"\",\"type\":\"clean\"}";
    private final SimpMessagingTemplate template;
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @Override
    public void send(final String topic, final String message) {
        template.convertAndSend(topic, message);
        if (!message.contains("\"type\":\"clean\"")) {
            executorService.schedule(() -> send(topic, CLEAN_MESSAGE), 20, TimeUnit.SECONDS);
        }
    }

}
