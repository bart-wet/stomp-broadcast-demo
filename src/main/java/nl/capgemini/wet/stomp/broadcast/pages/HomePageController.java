package nl.capgemini.wet.stomp.broadcast.pages;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
@RequiredArgsConstructor
public class HomePageController {

    @RequestMapping({"", "/"})
    public String index(final Model model) {
        return "index";
    }

}
