package nl.capgemini.wet.stomp.broadcast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class StompBroadcastDemoApplication {

	public static void main(final String[] args) {
		SpringApplication.run(StompBroadcastDemoApplication.class, args);
	}

}
