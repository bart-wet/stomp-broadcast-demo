package nl.capgemini.wet.stomp.broadcast;

public interface BroadcastService {

    void send(final String topic, String message);

}
