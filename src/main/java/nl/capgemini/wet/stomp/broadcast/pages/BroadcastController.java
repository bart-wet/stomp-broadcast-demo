package nl.capgemini.wet.stomp.broadcast.pages;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.capgemini.wet.stomp.broadcast.BroadcastService;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping("/message")
public class BroadcastController {

    private final BroadcastService service;

    @MessageMapping("/general")
    public void sendFormMessage(final @Payload String message) {
        log.info("Received message: {}", message);
        service.send("/topic/messages", message);
    }

    @RequestMapping({"", "/"})
    public String sendMessage(final Model model) {
        return "messageform";
    }

}
